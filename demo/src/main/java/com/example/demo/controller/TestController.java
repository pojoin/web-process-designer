package com.example.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/test")
public class TestController {

    @ResponseBody
    @RequestMapping("/str/{id}")
    public String test(@PathVariable("id") String name){
        return name+" ok";
    }

    @RequestMapping("/view")
    public ModelAndView view(){
        return new ModelAndView("test/view2");
    }

    @RequestMapping("/pro")
    public ModelAndView pro(){
        return new ModelAndView("pro/proManager");
    }

    @RequestMapping("/json")
    public ModelAndView json(){
        return new ModelAndView("pro/json");
    }

    @RequestMapping("/png")
    public ModelAndView png(){
        return new ModelAndView("pro/png");
    }

    @RequestMapping("/import")
    public ModelAndView importJson(){
        return new ModelAndView("pro/import");
    }
}
