# web-process-designer

#### 介绍
该项目是一个工作流的web在线版本的流程设计器，主要基于js绘图引擎draw2d.js实现，同时使用到了jquery相关js库、插件。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0607/131320_87dcdfb8_1172530.png "流程设计器示例图.png")

#### 软件架构
1.BS架构，IDE使用IntelliJ IDEA（utf-8编码）  
2.jdk基于1.8，maven用的3.5  
3.springboot+html+javascript  


#### 使用说明
1.项目启动后，直接访问[http://localhost:8083/test/pro](http://localhost:8083/test/pro)即可  
2.浏览器支持IE9及以上、chrome  


#### 功能支持
1.可视化拖拽设计流程图  
2.任务环节类型主要有：开始、结束、任务、网关、子流程  
3.支持折线  

#### 后续计划
1.基于bpmn2.0规范进行流程定义文件的生成，使用activiti-6进行流程部署与办理  
2.完善流程、任务的属性设置页面，已预留入口（双击事件触发）  
